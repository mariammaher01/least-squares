#include <iostream>
#include <vector>
#include <iomanip>
#include <cmath>
using namespace std;
double epsilon = 0.0001;
class Matrix;

class ColumnVector {
private :
    int row;
public:
    vector<double> val;
    /**
     * Constructor of a vector given its dimensions
     * @param n
     */
    ColumnVector(int n){
        val.resize(n);
        row = n;
        for(int i = 0; i < n ; i++)
            val[i] = 0;
    }

    /**
     * function that returns number of rows of the vector
     * @return dimension
     */

    int getRow() const {
        return row;
    }
    /**
     * function that override cin
     * @param in
     * @param v
     * @return
     */
    friend istream & operator >> (istream &in, ColumnVector &v) {
        for (int i = 0; i < v.getRow(); i++) {
            in >> v.val[i];
        }
        return in;
    }
    /**
     * function that override cout to output the matrix row by row
     * @param out
     * @param m
     * @return
     */
    friend ostream & operator << (ostream &out,const ColumnVector &v){
        for(int i = 0; i < v.row; i++){
            if(abs(v.val[i]) <= epsilon ) {
                out << "0.0000 " << endl;
                continue;
            }
            out << setprecision(4) <<  fixed << v.val[i];
            out << endl;
        }
        return out;
    }
    /**
     * function that override + of two vectors
     * @param v
     * @return
     */
    const ColumnVector operator +(const ColumnVector &v)
    {
        if(this->row != v.getRow()) {
            cout << "Error: the dimensional problem occurred" << endl;
            ColumnVector empty(0);
            return empty;
        }
        ColumnVector c(v.getRow());
        for(int i = 0 ; i < row; i++)
            c.val[i]= this->val[i] + v.val[i];
        return c;
    }
    /**
     * function that override the - operator of two vectors
     * @param m
     * @return a matric of the difference between two vectors
     */
    const ColumnVector operator -(const ColumnVector &v) {
        if(this->row != v.getRow()) {
            cout << "Error: the dimensional problem occurred" << endl;
            ColumnVector empty(0);
            return empty;
        }
        ColumnVector c(v.getRow());
        for (int i = 0; i < row; i++)
            c.val[i]= this->val[i] -v.val[i];
        return c;
    }
    /**
     * function that override * between two vectors
     * @param v
     * @return a double value of dot product
     */
    double operator * (const ColumnVector &v){
        if(this->row != v.getRow() ) {
            cout << "Error: the dimensional problem occurred" << endl;
            return NULL;
        }
        int r;
        r = this->row;
        double res = 0;
        for(int i = 0 ; i < r ;i++){
            res += this->val[i] * v.val[i];
        }
        return res;

    }
    /**
     * function that changes the vector to its norm
     */

    /*  void norm(){
          double len = 0;
          for(int i = 0; i < row; i++)
              len += val[i]*val[i];
          len = sqrt(len);
          for(int i = 0; i < row; i++)
              val[i] /= len;
      }*/
    double norm(){
        double len = 0;
        for(int i = 0; i < row; i++)
            len += val[i]*val[i];
        len = sqrtl(len);
        return len;
    }

};

class Matrix{
private:
    int row,col;
public:
    vector< vector<double> > val;
    /**
     * Matrix constructor that initialize
     * @param row
     * @param col
     */
    Matrix(int r, int c){
        this->row = r;
        this->col = c;
        val.resize(row);
        for(int a = 0; a < row; a++)
            val[a].resize(col);
        for(int i = 0; i < row; i++)
            for(int j = 0; j < col; j++)
                val[i][j] = 0;


    }
    /**
     * function returns number of columns of the matrix
     * @return column
     */
    int getCol () const {
        return col;
    }
    /**
     * function that returns number of rows of the matrix
     * @return row
     */
    int getRow() const {
        return row;
    }
    /**
     * function that override cin to input the matrix row by row
     * @param in
     * @param m
     * @return
     */
    friend istream & operator >> (istream &in, Matrix &m) {
        for (int i = 0; i < m.row; i++) {
            for (int j = 0; j < m.col; j++)
                in >> m.val[i][j];
        }
        return in;
    }
    /**
     * function that override cout to output the matrix row by row
     * @param out
     * @param m
     * @return
     */
    friend ostream & operator << (ostream &out,const Matrix &m){
        for(int i = 0; i < m.row; i++){
            for(int j = 0; j < m.col; j++) {
                if(abs(m.val[i][j]) <= epsilon ) {
                    out << "0.0000 ";
                    continue;
                }
                out << setprecision(4) << fixed << m.val[i][j] << " ";
            }
            out << endl;
        }
        return out;
    }
    /**
     * function that override = operation
     * @param m
     */
    const void operator =(const Matrix &m)
    {
        if(this->row != m.row || this->col != m.col) {
            cout << "Error: the dimensional problem occurred" << endl;
            return;
        }
        for(int i = 0 ; i < row; i++)
            for(int j = 0; j < col; j++)
                this->val[i][j] = m.val[i][j];
    }
    /**
     * function that ovveride + operation of two matrices
     * @param m
     * @return a matrix of the sum
     */
    const Matrix operator +(const Matrix &m)
    {
        if(this->row != m.row || this->col != m.col) {
            cout << "Error: the dimensional problem occurred" << endl;
            Matrix empty(0,0);
            return empty;
        }
        Matrix c(m.row,m.col);
        for(int i = 0 ; i < row; i++)
            for(int j = 0; j < col; j++)
                c.val[i][j] = this->val[i][j] + m.val[i][j];
        return c;
    }
    /**
     * function that override the - operator of two matrices
     * @param m
     * @return a matric of the difference between two matrices
     */
    const Matrix operator -(const Matrix &m) {
        if(this->row != m.row || this->col != m.col) {
            cout << "Error: the dimensional problem occurred" << endl;
            Matrix empty(0,0);
            return empty;
        }
        Matrix c(m.row, m.col);
        for (int i = 0; i < row; i++)
            for (int j = 0; j < col; j++)
                c.val[i][j] = this->val[i][j] - m.val[i][j];
        return c;
    }
    //5 * 3     3*4  = 5*4
    /**
     * function that override * operator
     * @param m
     * @return multiplication of two matrices
     */
    Matrix operator * (const Matrix &m){
        if(this->col != m.row ) {
            cout << "Error: the dimensional problem occurred" << endl;
            Matrix empty(0,0);
            return empty;
        }
        Matrix res(this->row,m.col);
        //cout << res.val[0][0];
        int c , r;
        r = this->row;
        c = m.col;

        for(int i = 0 ; i < r ;i++){
            for(int j = 0; j < c; j++){
                {
                    for(int k = 0; k < this->col ; k++)
                    {
                        res.val[i][j] += this->val[i][k] * m.val[k][j];
                    }
                }
            }
        }
        return res;

    }

    ColumnVector operator * (const ColumnVector &v){
        if(this->col != v.getRow() ) {
            cout << "Error: the dimensional problem occurred" << endl;
            ColumnVector empty(0);
            return empty;
        }
        /**
         * change to column vector
         */
        ColumnVector res(this->getRow());
        int  r;
        r = this->row;
        int c = this->col;

        for(int i = 0 ; i < r ;i++){
            for(int k = 0; k < c; k++)
            {
                res.val[i] += this->val[i][k] * v.val[k];
            }
        }
        return res;

    }
    /**
     * function that get a transpose of a given matrix
     * @return
     */
    Matrix transpose(){
        Matrix trans(this->col, this->row);
        for(int i = 0; i < this->row; i++){
            for(int j = 0; j < this->col; j++)
            {
                trans.val[j][i] = this->val[i][j];
            }
        }
        return trans;
    }

    /* Matrix(ColumnVector vector) {
         Matrix v(vector.getRow());
         for(int i = 0; i < vector.getRow(); i++){
             v.val[i][0] = vector.val[i];
         }
     }*/
    ColumnVector toVec(int col){
        col--;
        ColumnVector vec(this->getRow());
        for(int i = 0; i < this->getRow(); i++){
            vec.val[i] = val[i][col];
        }
        return vec;
    }
};

class SquareMatrix : public virtual Matrix{

public:
    /**
     * constructor of square matrix given its dimensions
     * @param n
     */
    SquareMatrix(int n) : Matrix(n, n) {
    }

};
class IdenityMatrix: public  SquareMatrix{
public:
    /**
     * constructor of Identity matrix given its dimensions
     * @param n
     */
    IdenityMatrix( int n) : Matrix(n,n), SquareMatrix(n) {
        for(int i = 0 ; i < n ; i ++)
            val[i][i] = 1;
    }
    /**
     * constructor of identity matrix given a matrix
     * @param matrix
     */

    IdenityMatrix(Matrix matrix) : SquareMatrix(matrix.getCol()), Matrix(matrix.getRow(), matrix.getCol()) {
        for(int i = 0 ; i < matrix.getCol() ; i ++)
            val[i][i] = 1;
    }
};
class EliminationMatrix:public IdenityMatrix{
public:
    /**
     * constructor of elimination matrix given position of the element to be eliminated and the matrix
     * @param i
     * @param j
     * @param m
     */
    EliminationMatrix(int i, int j ,Matrix m) : Matrix(m.getRow(), m.getCol()), IdenityMatrix(m.getRow()) {
        val[i][j] = -(m.val[i][j]/m.val[j][j]);

    }
};
class PermutationMatrix : public  IdenityMatrix{
public:
    /**
     * constrictor of a permutation matrix given two rows and a matrix
     * @param r1
     * @param r2
     * @param m
     */
    PermutationMatrix( int r1, int r2, Matrix m) : Matrix(m.getCol(), m.getRow()), IdenityMatrix(m.getRow()) {
        val[r1][r1] = 0;
        val[r2][r2] = 0;
        val[r1][r2] = 1;
        val[r2][r1] = 1;

    }
};



int findMaxPivot(int i, Matrix matrix);
//int findMaxPivotRev(int i, Matrix matrix);
void jacobi(SquareMatrix matrix, ColumnVector vector, double accuracy, int i);

bool diagonalDominance(SquareMatrix matrix);

double powD(double d, int p);

/**
 * Function that perform guassian elemenation of Ax=b and returns x
 * @param m
 * @param res
 * @return
 */
Matrix guassianElimination(Matrix m) {
    IdenityMatrix i(m.getCol());
    Matrix inv = i;

    int cnt = 1;
    for (int i = 0; i < m.getRow(); i++) {
        int r = findMaxPivot(i, m);
        if (r != i) {
            PermutationMatrix p(i, r, m);
            m = p * m;
            inv = p * inv;
            cnt++;
        }
        for (int k = i + 1; k < m.getRow(); k++) {
            if (!m.val[k][i]) continue;
            EliminationMatrix elm(k, i, m);
            Matrix mx(m.getRow(), m.getCol());
            m = elm * m;
            inv = elm * inv;

            cnt++;
        }
    }
    for (int i = m.getRow() - 1; i >= 0; i--) {
        /* int r = findMaxPivotRev(i, m);
         if(r != i) {
             cout << "step #" << cnt << ": permutation" << endl;
             PermutationMatrix p( i, r, m);
             m = p*m;
             inv = p*inv;
             cout << augment(m,inv);
             cnt++;
         }*/
        for (int k = i - 1; k >= 0; k--) {
            if (!m.val[k][i]) continue;
            EliminationMatrix elm(k, i, m);
            Matrix mx(m.getRow(), m.getCol());
            m = elm * m;
            inv = elm * inv;

            cnt++;
        }
    }
    ///norlmalize diagonals
    for (int i = 0; i < m.getRow(); i++) {
        for(int j= 0; j < inv.getCol(); j++)
            inv.val[i][j]/= m.val[i][i];
        m.val[i][i] = 1;
    }
    return inv;
}


/**
 * function that finds maximum pivot given row and a matrix
 * @param i
 * @param matrix
 * @return
 */
int findMaxPivot(int i, Matrix matrix) {
    double max = abs(matrix.val[i][i]);
    int pos = i;
    for(int a = i + 1; a < matrix.getRow(); a++ )
    {
        if(  abs(matrix.val[a][i]) - max  > epsilon ){
            max = abs(matrix.val[a][i]);
            pos = a;
        }
    }
    return pos;
}
int main(){
    int n ;
    cin >> n;
    Matrix z(n,2);
    cin >> z;
    int pol;
    cin >> pol;
    if (pol > 2) {
        cout << pol << endl;
        return 0;
    }
    ColumnVector b = z.toVec(2);
    ColumnVector t = z.toVec(1);

    Matrix a(n,pol + 1 );

/*if(pol == 1) {
    for (int i = 0; i < n; i++) {
        a.val[i][0] = 1;
        a.val[i][1] = t.val[i];
    }
}
else if(pol == 2){
    for (int i = 0; i < n; i++) {
        a.val[i][0] = 1;
        a.val[i][1] = t.val[i];
        a.val[i][2] = t.val[i]*t.val[i];
    }
}*/
    for(int i = 0; i < n; i++)
        for(int p = 0; p <= pol; p++)
        {
            a.val[i][p] = pow(t.val[i],p);
        }
    Matrix at = a.transpose();
    cout << "A:" << endl;
    cout << a;
    cout << "A_T*A:" <<  endl;
    Matrix m = at*a;
    cout << m ;
    cout << "(A_T*A)^-1:" << endl;
    Matrix m1 = guassianElimination(at*a);
    cout <<  m1;
    cout << "A_T*b:" << endl;
    ColumnVector v = at*b;
    cout << v;
    cout << "x~:" << endl;
    cout << m1*v;

}

double powD(double d, int p) {
    double res = d;
    if(p == 0) return 1;
    for(int i = 2; i < p; i++)
        res = res * d;
    return res;
}

